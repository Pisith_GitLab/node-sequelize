const express = require('express');
const bodyParser = require('body-parser');
require('dotenv').config();
const { dbConnection } = require('./dataSources/db');
const passport = require('passport');
const authRoute = require('./routes/authRoute');
const protectedRoute = require('./routes/protectedRoute');

const app = express();
const port = process.env.PORT || 3000;

// Initialize Passport and Passport JWT strategy
app.use(passport.initialize());
require('./config/passport')(passport);

// DB connection
dbConnection();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.status(200).json({message: 'Welcome to Node-Sequelize application'});
})

app.use('/api/v1/auth', authRoute);
app.use('/api/v1/pro', protectedRoute);

app.listen(port, () => {
  console.log(`Server is running on port ${port}.`);
})