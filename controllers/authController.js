const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const User = require('../models/user');
require('dotenv').config();

// Register a new user
const register = async (req, res) => {
  try {
    const { username, email, password } = req.body;
    // Generate a salt and hash the password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(password, salt);
    // Create a new user in the database
    const user = await User.create({ username, email, password: hashedPassword });
    res.json({ message: 'User registered successfully', user });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Failed to register user' });
  }
};

const login = async (req, res) => {
  try {
    const { email, password } = req.body;
    const user = await User.findOne({where: { email }});
    if (!user) {
      return res.status(404).json({ error: 'User not found' });
    }
    const isPasswordValid = await bcrypt.compare(password, user.password);
    if (!isPasswordValid) {
      return res.status(401).json({ error: 'Invalid password' });
    }
    // generate token
    const token = jwt.sign({sub: user.id}, process.env.JWT_SECRET_KEY, {expiresIn: '1h'})
    return res.status(200).json({message: 'Login successfully', token})
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Failed to login' });
  }
}


module.exports = {
  register,
  login
}