const protectedContent = async (req, res) => {
  res.status(200).json({message: 'This is protected content, accessable for only authorized user with token'})
}

module.exports = {
  protectedContent
}