const { sq } = require('../dataSources/db');
const { Model, DataTypes } = require('sequelize');

const User = sq.define('user', {
  id: {
    type: DataTypes.UUID,
    defaultValue: DataTypes.UUIDV4,
    primaryKey: true,
  },
  username: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  email: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
})

User.sync().then(() => {
  console.log('User model synced');
})

module.exports = User;