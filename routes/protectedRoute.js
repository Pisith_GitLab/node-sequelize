const express = require('express');
const protectedController = require('../controllers/protectedController');
const passport = require('passport');

const router = express.Router();

// Passport JWT authentication middleware
router.use(passport.authenticate('jwt', { session: false }));

router.get('/', protectedController.protectedContent);

module.exports = router;